mongoimport --db booking --collection addresses --file ../booking_database/addresses.json --jsonArray
mongoimport --db booking --collection apartments --file ../booking_database/apartments.json --jsonArray
mongoimport --db booking --collection bookings --file ../booking_database/bookings.json --jsonArray
mongoimport --db booking --collection destinations --file ../booking_database/destinations.json --jsonArray
mongoimport --db booking --collection hotels --file ../booking_database/hotels.json --jsonArray
mongoimport --db booking --collection villas --file ../booking_database/villas.json --jsonArray
mongoimport --db booking --collection locations --file ../booking_database/locations.json --jsonArray
mongoimport --db booking --collection users --file ../booking_database/users.json --jsonArray

