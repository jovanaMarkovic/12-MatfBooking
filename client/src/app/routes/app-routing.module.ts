import { AddHotelComponent } from './../add-hotel/add-hotel.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { LoginComponent } from '../login/login.component';
import { SerbiaDestinationsComponent } from '../serbia-destinations/serbia-destinations.component';
import { WorldDestinationsComponent } from '../world-destinations/world-destinations.component';
import { DestinationInfoComponent } from '../destination-info/destination-info.component';
import { MapComponent } from '../map/map.component';
import { BookingComponent } from '../booking/booking-villa/booking.component';
import { RegisterComponent } from '../register/register.component';
import { StaysComponent } from '../admin/stays/stays.component';
import { UsersComponent } from '../admin/users/users.component';
import { ProfileComponent } from '../profile/profile.component';
import { AddApartmentComponent } from './../add-apartment/add-apartment.component';
import { AddVillaComponent } from '../add-villa/add-villa.component';
import { BookingHotelComponent } from '../booking/booking-hotel/booking-hotel.component';
import { BookingApartmentComponent } from '../booking/booking-apartment/booking-apartment.component';
import { MyReservationsComponent } from '../my-reservations/my-reservations.component';
import { MyStaysComponent } from '../my-stays/my-stays.component';
import { ReservationsComponent } from '../reservations/reservations.component';

const routes: Routes = [
    {path: '', component: HomeComponent },
    {path: 'login', component: LoginComponent },
    {path: 'serbia-destinations', component: SerbiaDestinationsComponent },
    {path: 'destinations/:destinationId', component: DestinationInfoComponent },
    {path: 'world-destinations', component: WorldDestinationsComponent },
    {path: 'map', component: MapComponent},
    {path: 'booking/:id', component: BookingComponent},
    {path: 'booking-hotel/:hotelId', component: BookingHotelComponent},
    {path: 'booking-apartment/:apartmentId', component: BookingApartmentComponent},
    {path: 'register', component:RegisterComponent},
    {path: 'stays', component: StaysComponent},   
    {path: 'users', component: UsersComponent},    
    {path: 'profile', component: ProfileComponent},  
    {path: 'add-apartment', component: AddApartmentComponent},
    {path: 'add-villa', component: AddVillaComponent},
    {path: 'add-hotel', component: AddHotelComponent},
    {path: 'my-reservation', component: MyReservationsComponent},
    {path: 'my-stays', component: MyStaysComponent},
    {path: 'reservations', component: ReservationsComponent}
    
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
