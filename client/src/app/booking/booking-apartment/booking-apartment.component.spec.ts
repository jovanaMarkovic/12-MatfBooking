import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingApartmentComponent } from './booking-apartment.component';

describe('BookingApartmentComponent', () => {
  let component: BookingApartmentComponent;
  let fixture: ComponentFixture<BookingApartmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookingApartmentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingApartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
