import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {Observable, of} from "rxjs";
import {catchError, first} from "rxjs/operators";
import {mergeMap} from 'rxjs/operators';
import { FormGroup,FormBuilder, FormControl, Validators } from '@angular/forms';

import { DestinationsService } from '../../services/destinations.service';
import { BookingService } from '../../services/booking.service';
import { UserService } from '../../services/user.service';
import { MapService} from '../../services/map.service';

import { DestinationModel } from '../../models/destination.model';
import { ApartmentModel } from '../../models/apartment.model';
import { BookingModel, Booking } from '../../models/booking.model';
import { UserModel } from '../../models/user.model';
import { LocationModel } from '../../models/location.model';

@Component({
  selector: 'app-booking-apartment',
  templateUrl: './booking-apartment.component.html',
  styleUrls: ['./booking-apartment.component.css']
})
export class BookingApartmentComponent implements OnInit {
    public apartment: ApartmentModel;
    public destination: DestinationModel;
  
    public createReservationApartment: FormGroup;
    public bookings: BookingModel[];
  
   private paramMapSub: Subscription = null;
   
    public currentUser():UserModel{
  	  return this.userService.currentUser;
    }
    
    public latitude: number;
    public longitude: number;
 
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private destinationsService: DestinationsService,
        private bookingService: BookingService,
        private userService: UserService,
        private formBuilder: FormBuilder,
        private mapService: MapService) {
      
     this.paramMapSub = this.route.paramMap.subscribe(params => {
     
      const dId = params.get('destinationId');
      const id = params.get('apartmentId');
 
       this.destinationsService.getApartmentById(id)
        .subscribe((apartment: ApartmentModel) => {
            this.apartment = apartment;
            });
            
      this.destinationsService.getDestinationById(dId)
        .subscribe((destination: DestinationModel) => {
            this.destination = destination;
            });
    });

    this.createReservationApartment = this.formBuilder.group({
      guests:['', [Validators.required, Validators.min(1), Validators.max(100)]],
      startAt: [''],
      endAt: ['']
    });      
  };
    
  submitForm(){
    const data  = this.createReservationApartment.value;
    if (this.createReservationApartment.valid) {
      //alert('Form Submitted succesfully!!!\n Check the values in browser console.');
      console.log(data);
    }
    else
    {
      window.alert('Not valid');
      return;
    }

    
    const body = {
      "guests": data.guests,
      "startAt": data.startAt,
      "endAt": data.endAt,
      "rental": this.apartment._id
    }
    
    this.createReservationApartment.reset();
    
   
    const result:Observable<any>= this.bookingService.createBookingApartment(body);

    result.pipe(first(),catchError(err=>{
    window.alert(err.message);
    return of();
})).subscribe(object=>{
    window.alert(object.message);
});
    }
   
  ngOnInit(): void {
   
  };
 
   onMapReady(){
      this.latitude=this.apartment.location.width;
      this.longitude=this.apartment.location.height;
  }

}
