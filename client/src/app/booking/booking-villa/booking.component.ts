import { Component, OnInit, Input, ChangeDetectorRef} from '@angular/core';
import { DestinationsService } from '../../services/destinations.service';
import { BookingService } from '../../services/booking.service';
import { UserService } from '../../services/user.service';
import { MapService} from '../../services/map.service';

import { FormGroup,FormBuilder, FormControl, Validators } from '@angular/forms';

import { DestinationModel } from '../../models/destination.model';
import { HotelModel } from '../../models/hotel.model';
import { VillaModel } from '../../models/villa.model';
import { ApartmentModel } from '../../models/apartment.model';
import { BookingModel, Booking } from '../../models/booking.model';
import { UserModel } from '../../models/user.model';
import { LocationModel } from '../../models/location.model';


import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {Observable, of} from "rxjs";
import {catchError, first} from "rxjs/operators";

import {mergeMap} from 'rxjs/operators';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})

export class BookingComponent implements OnInit {
   
    public villa: VillaModel;
    public destination: DestinationModel;

    public createReservationVilla: FormGroup;
    public bookings: BookingModel[];
  
   private paramMapSub: Subscription = null;
   
    public currentUser():UserModel{
  	  return this.userService.currentUser;
    }
    
    public latitude: number;
    public longitude: number;
 
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private destinationsService: DestinationsService,
        private bookingService: BookingService,
        private userService: UserService,
        private formBuilder: FormBuilder,
        private mapService: MapService,
        private ref: ChangeDetectorRef) {
      
     this.paramMapSub = this.route.paramMap.subscribe(params => {

      const dId = params.get('destinationId');
      const id = params.get('id');
 
      this.destinationsService.getVillaById(id)
        .subscribe((villa: VillaModel) => {
            this.villa = villa;
        });
      
      this.destinationsService.getDestinationById(dId)
        .subscribe((destination: DestinationModel) => {
            this.destination = destination;
            });
    });

    this.createReservationVilla = this.formBuilder.group({
      guests:['', [Validators.required, Validators.min(1), Validators.max(100)]],
      startAt: [''],
      endAt: ['']
    });      
  };
    
    
  submitForm(){
    const data  = this.createReservationVilla.value;
    if (this.createReservationVilla.valid) {
      console.log(data);
    }
    else
    {
      window.alert('Not valid');
      return;
    }

    const body = {
      "guests": data.guests,
      "startAt": data.startAt,
      "endAt": data.endAt,
      "rental": this.villa._id
    }
    
    this.createReservationVilla.reset();
   
    const result:Observable<any>= this.bookingService.createBookingVilla(body);

    result.pipe(first(),catchError(err=>{
    window.alert(err.message);
    return of();
})).subscribe(object=>{
    window.alert(object.message);
});
    }
   
  ngOnInit(): void {
   
  };
 
   onMapReady(){
      this.latitude=this.villa.location.width;
      this.longitude=this.villa.location.height;
  }   
}



