import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import {DestinationsService } from '../services/destinations.service';
import {DestinationModel } from '../models/destination.model';


@Component({
  selector: 'app-serbia-destinations',
  templateUrl: './serbia-destinations.component.html',
  styleUrls: ['./serbia-destinations.component.css']
})
export class SerbiaDestinationsComponent implements OnInit {

    public destinations: DestinationModel[];
    
  constructor(private destinationsService: DestinationsService) {
    this.destinationsService.getDestinations()
        .subscribe((destinations: DestinationModel[]) => {
            this.destinations = destinations;
        });
   }

  ngOnInit(): void {
  }

}
