import { Component, OnInit } from '@angular/core';
import { UserModel } from '../models/user.model';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute} from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {
  
   ngOnInit(): void {
   };
   
   public currentUser():UserModel{
  	    return this.userService.currentUser;
  }
  
  public logout():void{
  	this.userService.removeCurrentUser();
  	this.userService.destroyToken();
  	this.router.navigate(['/']);
  	
  }

  

  constructor(private userService:UserService,private router: Router, private route: ActivatedRoute) { }

 
}
