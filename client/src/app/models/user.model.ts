import { HotelModel } from "./hotel.model";
import { ApartmentModel } from "./apartment.model";
import { VillaModel, Villa } from "./villa.model";
import { BookingModel, Booking } from "./booking.model";


export interface UserModel {
    _id: string;
    username: string;
    password: string;
    email: string;
    name: string;
    surname: string;
    gender: string;
    userRole: string;
    rentals: VillaModel[] | ApartmentModel[] | HotelModel[];
    bookings: Booking[];
}
