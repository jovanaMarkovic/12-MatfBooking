import { UserModel } from './user.model';
import { LocationModel } from "./location.model";
import { BookingModel, Booking } from './booking.model';


export interface HotelModel {
    _id: string;
    hotelName: string;
    numberOfRooms: number; 
    numOfGuests: number;
    location: LocationModel;
    destination: string;
    rentingDate: string;
    dateAvailable: string;
    user: string;
    comments: string[];
    images: string;
    pricePerNight: string;
    checkIn: string;
    checkOut: string;
    status: string;
    amenities: string[];
    reservations: Booking[];
}


export class Hotel {
    _id: string;
    user: string | UserModel;
    hotelName: string;
    numberOfStars: number;
    numberOfRooms: number; 
    numOfGuests: number;
    location:string | LocationModel;
    destination: string;
    pricePerNight: string;
    images: string;
    reservations: Booking[];
}
