import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs'; 
import { ActivatedRoute, Router } from '@angular/router';
import { DestinationsService } from '../../services/destinations.service';

import { DestinationModel } from '../../models/destination.model';
import { HotelModel } from '../../models/hotel.model';
import { VillaModel } from '../../models/villa.model';
import { ApartmentModel } from '../../models/apartment.model';

@Component({
  selector: 'app-stays',
  templateUrl: './stays.component.html',
  styleUrls: ['./stays.component.css']
})
export class StaysComponent implements OnInit {

  public destinations: DestinationModel[];
  public hotels: HotelModel[];
  public villas: VillaModel[];
  public apartments: ApartmentModel[];


    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private destinationsService: DestinationsService,
        ){
        
        this.destinationsService.getDestinations()
          .subscribe((destinations: DestinationModel[]) => {
            this.destinations = destinations;
            });        
        
          
        this.destinationsService.getHotels()
          .subscribe((hotels: HotelModel[]) => {
            this.hotels = hotels;
            });        
        
        this.destinationsService.getApartments()
          .subscribe((apartments: ApartmentModel[]) => {
            this.apartments = apartments;
            });        
        
        this.destinationsService.getVillas()
          .subscribe((villas: VillaModel[]) => {
            this.villas = villas;
            });        
   }
  ngOnInit(): void {
  }

}
