import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs'; 
import { ActivatedRoute, Router } from '@angular/router';

import { BookingService } from '../services/booking.service';
import { BookingModel } from '../models/booking.model';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.css']
})
export class ReservationsComponent implements OnInit {

  public bookings: BookingModel[];
  
  constructor(
        private route: ActivatedRoute,
        private router: Router,
        private bookingService: BookingService) { 
         this.bookingService.getBookings()
          .subscribe((booking: BookingModel[]) => {
            this.bookings = booking;
            });        
  }

  ngOnInit(): void {
  }

}
