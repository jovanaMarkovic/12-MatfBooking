const mongoose =require('mongoose');
const Location = require("../models/locations");
const User = require("../models/user");


const hotelsSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    hotelName: {
        type: String,
        required: true, 
        default: '', 
        trim: true
    },
    numberOfStars:{
        type: Number, 
        required: true,
        default: 0, 
        trim: true
    },
    numberOfRooms:{
        type: Number,
        required: true, 
        default: 0, 
        trim: true
    },
    numOfGuests: {
        type: Number,
        required: true, 
        default: 0, 
        trim: true
    },
    location:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Location'
    },
    destination:{
        type:String,
        defualt:''
    },/*
    rentingDate:{
        type: Date,
        required: true,
        default: Date.now, 
        trim: true
    },
    dateAvailable:{
        type: Date,
        required: true,
        default: Date.now, 
        trim: true
    },*/
    user:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'User'
    },/*
    comments:{
        type: [String],
        required: true,
        default: [], 
        trim: true
    },*/
    images:{
        type:String,
        /*required: true,*/
        default: '', 
        trim: true
    },
    pricePerNight:{
        type:String,
        required: true,
        default: [], 
        trim: true
    }/*,
    checkIn:{
        type: String,
        required: true,
        default: '', 
        trim: true
    },
    checkOut:{
        type: String,
        required: true,
        default: '', 
        trim: true
    },
    status:{
        type: Date,
        required: true,
        default: 'active', 
        trim: true
    },//TODO add this arrays
    amenities:{
        type:[String],
        required: true,
        default: [], 
        trim: true
    }*/,
    reservations:[{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Booking',
        default: [], 
        trim: true
    }]
       
});

module.exports =mongoose.model('Hotel',hotelsSchema);
