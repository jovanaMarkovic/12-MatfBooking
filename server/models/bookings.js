const mongoose =require('mongoose');
const User = require("../models/user");
const Apartment = require('../models/apartments')
const Hotel = require('../models/hotels')
const Villa = require('../models/villas')

const bookingsSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    startAt: {
        type: Date,
       // required: [true, 'Starting date is required'],
        default: Date.now, 
        trim: true
    },
    endAt: {
        type: Date,
      //  required: [true, 'Ending date is required'],
        default: Date.now, 
        trim: true
    },
    totalPrice: Number,
    days: Number,
    guests: Number,
    createdAt: {type: Date, default: Date.now},
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'},
    rental: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Villa' || 'Hotel' || 'Apartment'
        }
/*
    numOfGuest: {
        type: Number,
        default: 1, 
        trim: true
    }
    amount: {
        type: Number,
      //  required: [true, 'Total amount is required'],
        default: 10,
        trim: true
    },
  
    guests: {
        type: Number,
        required: [true, 'Number of guests is required'],
        default: 1, 
        trim: true
    },
    nights: {
        type: Number,
      //  required: [true, 'Number of nights is required'],
        default: 1, 
        trim: true
    },
    createdBy: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    message: {
        type: String,
        required: true,
        default: '', 
        trim: true
    },
    status:{
        type: String,
        required: true,
        default: '', 
        trim: true
  }
    */
});

module.exports =mongoose.model('Booking', bookingsSchema);
