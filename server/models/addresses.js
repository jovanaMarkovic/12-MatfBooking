const mongoose =require('mongoose');

const addressesSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    streetAndNumber: {
        type: String,
        required: true, 
        default: '', 
        trim: true
    },
    postCode: {
        type: String,
        required: true,
        default: '', 
        trim: true
    }
    
});

module.exports =mongoose.model('addresses',addressesSchema);
