const mongoose = require('mongoose');
const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt=require('jsonwebtoken');
const {ExtractJwt}=require('passport-jwt');

module.exports.getUsers = async (req, res, next) => {
  try {
    const users = await User.find({}).exec();
    res.status(200).json(users);
  } catch (err) {
    next(err);
  }
};


module.exports.getUserById = async (req, res, next) => {
  const id = req.user._id;

  try {
    if (id<0) {
      const error = new Error('Ne postoji taj id');
      error.status = 400;
      throw error;
    }

    const user = await User.findById(id).populate('bookings').exec();
    if (user == null) {
      res.status(404).json();
    } else {
      res.status(200).json(user);
    }
  } catch (error) {
    next(error);
  }
};

module.exports.getUserByUsername = async (req, res, next) => {
  const username = req.params.username;

  try {
    if (username == undefined) {
      const error = new Error('Nedostaje korisnicko ime!');
      error.status = 400;
      throw error;
    }

    const user = await User.find({username: username}).exec();
    if (user == null) {
      res.status(404).json();
    } else {
      res.status(200).json(user);
    }
  } catch (error) {
    next(error);
  }
};

module.exports.addNewUser = async function (req, res, next) {
  const userObject = {
    _id: new mongoose.Types.ObjectId(),
    username: req.body.username,
    password: await bcrypt.hash(req.body.password,10),//HASH password kad pravis novog usera
    email: req.body.email,
    name: req.body.name,
    surname: req.body.surname,
    gender: req.body.gender,
    userRole: req.body.userRole
    };
 
  const user = new User(userObject);

  try {
    const savedUser = await user.save();
    res.status(201).json({
      message: 'A guest is successfully created',
      user: savedUser,
    });
  } catch (err) {
    next(err);
  }
};

module.exports.deleteUser = async function (req, res, next) {
  const username = req.params.username;

  try {
    await User.deleteOne({ username: username }).exec();
    res.status(200).json({ message: 'The user is successfully deleted' });
  } catch (err) {
    next(err);
  }
};

module.exports.updateUserPassword = async function (req, res, next) {
    try{
        const username = req.params.username;
        const user = await User.find({username: username}).exec();
   
        if (user == null) {
            res.status(404).json();
        } else {
     
            const updatePassword = req.body;
            await User.updateOne({username: username}, {$set: {password: updatePassword.newPassword}}).exec();
            res.status(200).json({
                message: "Uspesno je azuriran korisnik!"
            });
        }  
     }catch(error){
        next(error);
     }
};

module.exports.login = async function (req,res,next) {

   const {email, password} =req.body;
   
   if(email && password){
     const user = await User.findOne({email:email}).exec();
     
   if(!user)
     return res.status(401).json({message:"No such user"})

   if(await bcrypt.compare(password,user.password)){
     
     const token = jwt.sign({id:user._id},"5105204425768ee9debc645695618ff5407e4ffe3057d2d5fd");
     user.password=undefined;
     user.__v=undefined;
     return res.status(200).json({message:"Login successs,",token:token,user:user});
     
   
   }
    return res.status(401).json({message:"Wrong password"});

 }
 
}
module.exports.getCurrentUser = async function (req,res,next) {

   if(!req.user)
            return res.status(404).json({"message":"Not logged in"});
   const user=req.user;
   user.password=undefined;
   user.__v=undefined;        
   return res.status(200).json(user);

}


module.exports.changeUserInfoData = async (req, res, next) => {
   try {
        if (!req.params.id || !mongoose.Types.ObjectId.isValid(req.params.id)) {
            res.status(400).json({
                message: "Invalid id"
            });
        } else if (!req.body.username || !req.body.email) {
            res.status(400).json({
                message: "Missing username or email"
            });
        } else {
            const user = await User.findById({_id: req.params.id}).exec();
            if (!user) {
                res.status(400).json({
                    message: "Wrong id"
                });
            } else {
                user.username = req.body.username;
                user.email = req.body.email;
                
                await User.findOneAndUpdate({
                    _id: req.params.id
                }, {
                     username: user.username,
                     email: user.email
                }).exec();

                res.status(200).json({
                message: "Update success!"
            });

            }
        }
    } catch(err) {
        next(err);
    }
}
