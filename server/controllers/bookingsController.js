const mongoose = require('mongoose');
const Booking = require('../models/bookings');
const Villa = require('../models/villas');
const Hotel = require('../models/hotels');
const Apartment = require('../models/apartments');
const User = require('../models/user');
const moment = require('moment');

module.exports.getBookings = async (req, res, next) => {
  try {
    const bookings = await Booking.find({}).populate('rental').populate('user').exec();
    res.status(200).json(bookings);
  } catch (err) {
    next(err);
  }
};

module.exports.getBookingById = async (req, res, next) => {
  const id = req.params.id;

  try {
    if (id<0) {
      const error = new Error('Not existing id');
      error.status = 400;
      throw error;
    }

    const booking = await Booking.findById(id).exec();
    if (booking == null) {
      res.status(404).json();
    } else {
      res.status(200).json(booking);
    }
  } catch (error) {
    next(error);
  }
};

module.exports.createBookingVilla = async function (req, res, next) {
    
    const {startAt, endAt, guests,  rental} = req.body;
  // const user = req.body.user;
   const user = req.user._id;
    
    const _id = new mongoose.Types.ObjectId();
    const booking = new Booking({_id, startAt, endAt, guests, user});
   
    const villaObj = await Villa.findById(rental).populate('reservations').populate('user').exec();
    if(villaObj == null){
        res.status(404).json({
                 message: 'Villa is dont exists created'})
    }else{
        if(villaObj.numOfGuests < guests){
            res.status(404).json({
                 message: 'Not valid num of guests!'})
        }
        /*else{
            Villa.updateOne({_id: rental}, {numOfGuests:  (villaObj.numOfGuests - guests)}, function(){})
    } */
    
     var oneDay=1000 * 60 * 60 * 24;
    // var difference_ms = Math.abs(end.getTime() - start.getTime())
     var days = Math.round((booking.endAt - booking.startAt) / oneDay);
       // console.log((villaObj.numOfGuests - guests));
    // console.log(villaObj);       
    // console.log(isValidBooking(booking, villaObj));
        /*
        if (villaObj.numOfGuests < guests){
            res.status(404).json({
                 message: 'Previse gostiju, nema mesta!'})
        }
      */
        if(isValidBooking(booking, villaObj)){
            booking.rental = villaObj._id;
            booking.days=days;
            booking.totalPrice = villaObj.pricePerNight * days;
          
           console.log(booking);        
         /*   User.update({_id: user}, {$push: {bookings: booking._id}}, function(){})
          */
          User.updateOne({_id: user}, {$push: {bookings: booking._id}}, function(){})
          
            Villa.updateOne({_id: rental}, {$push: {reservations: booking._id}}, function(){})

            try{
                const savedBooking=await booking.save();
                res.status(201).json({
                 message: 'Villa is successfully booked',
                 booking: savedBooking,
                 villa: villaObj,
                });
            } catch(err){
                next(err);
            }    
        }
        else{
        res.status(400).json({
                 message: 'Ne mozes da reservises, vec je reyervisana za taj datum'
                });
       }
}
}

module.exports.createBookingHotel = async function (req, res, next) {
    
    const {startAt, endAt, guests, rental} = req.body;
  // const user = req.body.user;
   const user = req.user._id;
    
    const _id = new mongoose.Types.ObjectId();
    const booking = new Booking({_id, startAt, endAt, guests, user});
   
    const hotelObj = await Hotel.findById(rental).populate('reservations').populate('user').exec();
    if(hotelObj == null){
        res.status(404).json({
                 message: 'Villa is dont exists created'})
    }else{
    
        if(hotelObj.numOfGuests < guests){
            res.status(404).json({
                 message: 'Not valid num of guests!'})
        } 
          else{
            Hotel.updateOne({_id: rental}, {numOfGuests:  (hotelObj.numOfGuests - guests)}, function(){})
    } 
    
     var oneDay=1000 * 60 * 60 * 24;
    // var difference_ms = Math.abs(end.getTime() - start.getTime())
     var days = Math.round((booking.endAt - booking.startAt) / oneDay);
     //console.log(hotelObj);       
    // console.log(isValidBooking(booking, hotelObj));
        
        if(isValidBooking(booking, hotelObj)){
            booking.rental = hotelObj._id;
            booking.days=days;
            booking.totalPrice=hotelObj.pricePerNight * days;  
          User.updateOne({_id: user}, {$push: {bookings: booking._id}}, function(){})
          
            Hotel.updateOne({_id: rental}, {$push: {reservations: booking._id}}, function(){})

            try{
                const savedBooking=await booking.save();
                res.status(201).json({
                 message: 'Hotell is successfully booked',
               //  booking: savedBooking,
               //  hotel: hotelObj,
                });
            } catch(err){
                next(err);
            }    
        }
        else{
        res.status(400).json({
                 message: 'Ne mozes da reservises, vec je reyervisana za taj datum'
                });
       }
}
}

module.exports.createBookingApartment = async function (req, res, next) {
    
    const {startAt, endAt, guests, rental} = req.body;
  // const user = req.body.user;
   const user = req.user._id;
    
    const _id = new mongoose.Types.ObjectId();
    const booking = new Booking({_id, startAt, endAt, guests, user});
   
    const apartmentObj = await Apartment.findById(rental).populate('reservations').populate('user').exec();
    if(apartmentObj == null){
        res.status(404).json({
                 message: 'Villa is dont exists created'})
    }else{
    
        if(apartmentObj.numOfGuests < guests){
            res.status(404).json({
                 message: 'Not valid num of guests!'})
        } 
         else{
            Apartment.updateOne({_id: rental}, {numOfGuests:  (apartmentObj.numOfGuests - guests)}, function(){})
    } 
     
     var oneDay=1000 * 60 * 60 * 24;
    // var difference_ms = Math.abs(end.getTime() - start.getTime())
    var days = Math.round((booking.endAt - booking.startAt) / oneDay);

   //  console.log(hotelObj);       
   //  console.log(isValidBooking(booking, hotelObj));
        
        if(isValidBooking(booking, apartmentObj)){
            booking.rental = apartmentObj._id;
            booking.days=days;
            booking.totalPrice = apartmentObj.pricePerNight * days;
          
            User.updateOne({_id: user}, {$push: {bookings: booking._id}}, function(){})
          
            Apartment.updateOne({_id: rental}, {$push: {reservations: booking._id}}, function(){})

            try{
                const savedBooking=await booking.save();
                res.status(201).json({
                 message: 'Apartment is successfully booked',
               //  booking: savedBooking,
               //  hotel: hotelObj,
                });
            } catch(err){
                next(err);
            }    
        }
        else{
        res.status(400).json({
                 message: 'Ne mozes da reservises, vec je reyervisana za taj datum'
                });
       }
}
}

function isValidBooking(proposedBooking, rental){
    let isValid = true;
 
    if(rental.reservations && rental.reservations.length > 0){
        isValid = rental.reservations.every(function(reservations)  
        {
            const proposedStart = moment(proposedBooking.startAt);
            const proposedEnd = moment(proposedBooking.endAt);
        //    console.log(proposedStart);
        //    console.log(proposedEnd);

            const actualStart = moment(reservations.startAt);
            const actualEnd = moment(reservations.endAt);
        //    console.log(actualStart);
        //    console.log(actualEnd);

            return ((actualStart < proposedStart && actualEnd < proposedStart) || (proposedEnd< actualEnd && proposedEnd < actualStart))
            }
        )
    }
   
    return isValid;
}
