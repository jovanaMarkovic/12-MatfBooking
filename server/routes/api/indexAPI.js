const express = require('express');

const router = express.Router();

router.get('/', function (req, res, next) {
  const api = [
      { path: '/', children: [] },
      { path: '/destinations', children: [
          { path: '/', method: 'GET', children: [] },
        //  { path: '/', method: 'POST', children: [] },
          { path: '/', method: 'GET', parameters: ['destinationId'], children: [] }
       //   { path: '/', method: 'PATCH', parameters: ['destinationId'], children: [] },
       //   { path: '/', method: 'DELETE', parameters: ['destinationId'], children: [] }
      ] },
      { path: '/users', children: [
          { path: '/', method: 'GET', children: [] },
        //  { path: '/', method: 'POST', children: [] },
       //   { path: '/', method: 'PATCH', parameters: [], children: [] },
       //   { path: '/', method: 'DELETE', parameters: [], children: [] }
      ] }
      
      /*,
      { path: '/order', children: [
        { path: '/', method: 'GET', children: [] },
        { path: '/', method: 'POST', children: [] },
        { path: '/', method: 'GET', parameters: ['destinationId'], children: [] },
        { path: '/', method: 'DELETE', parameters: ['destinationId'], children: [] }
      ] },
      */
  ];
  res.status(200).json(api);
});

module.exports = router;
