const express=require('express');
const router=express.Router();


const controller = require('../../controllers/addressesController');

//http:://localhost:3000/api/addresses
router.get('/', controller.getAddresses);

module.exports = router;
